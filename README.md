# Madrid Housing Demo on CP4D

![folium](https://gitlab.com/whendrik/madrid-demo-on-cp4d/-/raw/master/images/madrid_demo.gif)

## Instructions

1. Projects > New Project > Analytics Project
2. Create an empty project
3. Name it `Madrid Housing Demo` or similar (free choice, no effect on later steps)
4. Add to Project > Data, and upload `unique_metro.csv` & `madrid_houses.csv`. Both files can be found in this git project.
5. Add to Project > Notebook > From File > `Madrid Housing Demo.ipynb`. File can be found in this git project.

The Notebook will guide you to ultimately

- Create a Model
- Create a data-set suitable for Auto-AI Experiment
- Deploy the Model
- Run an example REST Call

### Example REST Call for Housing Demo

```
{"input_data": [{"fields": ["property_state", "distance_to_centre", "distance_to_metro", "mts2"], 
"values": [[1,7,1,120]]}]}
```
